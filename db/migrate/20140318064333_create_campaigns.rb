class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :name
      t.datetime :date
      t.string :location

      t.timestamps
    end
  end
end
