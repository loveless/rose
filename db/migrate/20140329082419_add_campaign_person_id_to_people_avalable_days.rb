class AddCampaignPersonIdToPeopleAvalableDays < ActiveRecord::Migration
  def change
    remove_column :people_available_days, :person_id
    add_column :people_available_days, :campaign_person_id, :integer
  end
end
