class CreateCampaignStaffs < ActiveRecord::Migration
  def change
    create_table :campaign_staffs do |t|
      t.integer :campaign_id
      t.integer :staff_id

      t.timestamps
    end
  end
end
