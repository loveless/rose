class AddStatusToKlasses < ActiveRecord::Migration
  def change
    add_column :klasses, :status, :string
  end
end
