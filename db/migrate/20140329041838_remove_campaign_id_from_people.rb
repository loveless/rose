class RemoveCampaignIdFromPeople < ActiveRecord::Migration
  def change
    remove_column :people, :campaign_id
  end
end
