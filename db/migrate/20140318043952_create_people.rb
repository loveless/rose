class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :type
      t.string :gender
      t.datetime :join_date
      t.integer :campaign_id

      t.timestamps
    end
  end
end
