class CreateCampaignPeople < ActiveRecord::Migration
  def change
    create_table :campaign_people do |t|
      t.integer :campaign_id
      t.integer :person_id
      t.boolean :is_contacted
      t.string :note

      t.timestamps
    end
  end
end
