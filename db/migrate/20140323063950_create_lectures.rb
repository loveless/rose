class CreateLectures < ActiveRecord::Migration
  def change
    create_table :lectures do |t|
      t.string :name
      t.datetime :start_time
      t.datetime :finish_time
      t.integer :klass_id
      t.string :status

      t.timestamps
    end
  end
end
