class CreatePeopleAvailableDays < ActiveRecord::Migration
  def change
    create_table :people_available_days do |t|
      t.integer :person_id
      t.integer :m_day_id

      t.timestamps
    end
  end
end
