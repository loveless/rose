class CreateMDays < ActiveRecord::Migration
  def change
    create_table :m_days do |t|
      t.string :name
      t.string :short_name
      t.string :short_name_vi

      t.timestamps
    end
  end
end
