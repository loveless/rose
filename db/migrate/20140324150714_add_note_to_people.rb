class AddNoteToPeople < ActiveRecord::Migration
  def change
    add_column :people, :note, :string
  end
end
