class Student < Person
  has_many :student_klasses
  has_many :klasses, through: :student_klasses

  accepts_nested_attributes_for :student_klasses, allow_destroy: true
end
