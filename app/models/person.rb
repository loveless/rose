class Person < ActiveRecord::Base
  has_many :campaign_people
  has_many :campaigns, through: :campaign_people

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  VALID_PHONE_REGEX = /\A[0-9\-\+]{9,15}\z/i

  validates :name, presence: true
  validates :email, :phone, allow_blank: true, uniqueness: {case_sensitive: false}
  validates :email, allow_blank: true, format: {with: VALID_EMAIL_REGEX}
  validates :phone, allow_blank: true, format: {with: VALID_PHONE_REGEX}

  before_validation :format_phone_number
  before_validation :format_email

  accepts_nested_attributes_for :campaign_people, allow_destroy: true

  def has_email?
    self.email.present?
  end

  def has_phone?
    self.phone.present?
  end

  def is_student?
    self.type == Student.name
  end

  def format_phone_number
    if self.phone.present?
      self.phone = self.phone.scan(/\d/).join
      self.phone = "0#{self.phone}" unless self.phone.start_with? "0"
    end
  end

  def format_email
    self.email = self.email.downcase if self.email.present?
  end
end
