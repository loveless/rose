class Campaign < ActiveRecord::Base
  has_many :campaign_staffs
  has_many :staffs, through: :campaign_staffs
  has_many :campaign_people
  has_many :people, through: :campaign_people

  validates :name, :location, :date, presence: true
end
