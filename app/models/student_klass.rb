class StudentKlass < ActiveRecord::Base
  belongs_to :student
  belongs_to :klass

  validates_uniqueness_of :student_id, scope: :klass_id
end
