class Staff < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :registerable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable,
    :validatable

  has_many :campaign_staffs
  has_many :campaigns, through: :campaign_staffs
end
