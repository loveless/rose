class PeopleAvailableDay < ActiveRecord::Base
  belongs_to :campaign_person
  belongs_to :m_day, class_name: M::Day
end
