class CampaignPerson < ActiveRecord::Base
  belongs_to :campaign
  belongs_to :person
  has_many :people_available_days
  has_many :m_days, through: :people_available_days, class_name: M::Day
end
