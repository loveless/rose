class M::Day < ActiveRecord::Base
  has_many :people_available_days, foreign_key: :m_day_id
  has_many :campaign_people, through: :people_available_days
end
