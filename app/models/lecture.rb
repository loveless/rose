class Lecture < ActiveRecord::Base
  belongs_to :klass

  validates :name, :klass_id, presence: true
end
