class Manage::StaffsController < Manage::BaseController
  before_action :require_admin

  def index
    @staffs = Staff.all
  end

  def new
  end

  def create
    @staff = Staff.new staff_params
    @staff.password = Devise.friendly_token.first 8
    if @staff.save
      redirect_to manage_staffs_path
    else
      render :new
    end
  end

  def edit
    @staff = Staff.find params[:id]
  end

  def update
    @staff = Staff.find params[:id]
    if @staff.update staff_params
      redirect_to manage_staffs_path
    else
      flash[:error] = "Cannot update staff info. Please try again."
      render :edit
    end
  end

  private
  def staff_params
    params.require(:staff).permit :name, :email, :type
  end

  def require_admin
    unless current_staff.type == Staff::Admin.name
      flash[:error] = "You don't have permission to access this page!"
      redirect_to manage_root_path
    end
  end
end
