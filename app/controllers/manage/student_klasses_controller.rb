class Manage::StudentKlassesController < Manage::BaseController

  def index
    @klass = Klass.find params[:klass_id]
    @student_klasses = @klass.student_klasses
    @q = Person.search params[:q]
    if params[:q]
      @people = @q.result(distinct: true) - @klass.students
    else
      @people = Array.new
    end
  end

  def create
    student = Person.find student_klass_params[:student_id]
    unless student.type == Student.name
      student.update type: Student.name
    end
    StudentKlass.create student_klass_params
    redirect_to manage_klass_student_klasses_path
  end

  def destroy
    student_klass = StudentKlass.find params[:id]
    student_klass.destroy
    redirect_to manage_klass_student_klasses_path
  end

  private
  def student_klass_params
    params.require(:student_klass).permit :student_id, :klass_id
  end
end
