class Manage::LecturesController < Manage::BaseController

  def index
    @klass = Klass.find params[:klass_id]
    @lectures = @klass.lectures
  end

  def new
    @klass = Klass.find params[:klass_id]
  end

  def create
  end

  def edit
  end

  def update
  end

  def destroy
  end

  def show
  end
end
