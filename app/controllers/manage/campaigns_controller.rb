class Manage::CampaignsController < Manage::BaseController

  def index
    @campaigns = Campaign.all.sort_by{|c| c.date}.reverse
  end

  def show
    @campaign = Campaign.find params[:id]
    @days = M::Day.all
    @q = Person.search params[:q]
    if params[:q]
      @people = @q.result(distinct: true) - @campaign.people
    else
      @people = Array.new
    end
  end

  def new
    @campaign = Campaign.new
  end

  def create
    @campaign = Campaign.new campaign_params
    if @campaign.save
      flash.now[:success] = "Campaign created. Now add people to campaign."
      redirect_to manage_campaign_path @campaign
    else
      render :new
    end
  end

  def edit
    @campaign = Campaign.find params[:id]
  end

  def update
    @campaign = Campaign.find params[:id]
    if @campaign.update!(campaign_params)
      flash.now[:success] = "Campaign created. Now add people to campaign."
      redirect_to manage_campaign_path @campaign
    else
      flash.now[:error] = "Cannot create campaign. Please try again!"
      render :new
    end
  end

  def destroy
  end

  private
  def campaign_params
    params.require(:campaign).permit :name, :location, :date, :description,
      staff_ids: []
  end
end
