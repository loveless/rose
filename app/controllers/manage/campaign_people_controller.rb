class Manage::CampaignPeopleController < Manage::BaseController

  def create
    c = CampaignPerson.create campaign_person_params
    if c.save
      redirect_to edit_manage_campaign_person_path c.campaign_id, c.person_id
    else
      flash[:error] = "Cannot add the person to this campaign"
      redirect_to manage_campaign_path c.cmapaign_id
    end
  end

  def update
    camp_person = CampaignPerson.find params[:id]
    camp_person.update is_contacted: params[:is_contacted] if params[:is_contacted]
    redirect_to manage_campaign_path camp_person.campaign_id
  end

  def destroy
    camp_person = CampaignPerson.find params[:id]
    campaign = camp_person.campaign
    camp_person.destroy!
    redirect_to manage_campaign_path campaign
  end

  private
  def campaign_person_params
    params.require(:campaign_person).permit :campaign_id, :person_id
  end
end
