class Manage::KlassesController < Manage::BaseController

  def index
    @klasses = Klass.all
  end

  def show
    @klass = Klass.find params[:id]
  end

  def new
    @klass = Klass.new
  end

  def create
    @klass = Klass.new klass_params   
    if @klass.save
      redirect_to manage_klass_path @klass
    else
      render :new
    end
  end

  def edit
    @klass = Klass.find params[:id]
  end

  def update
    @klass = Klass.find params[:id]
    if @klass.update klass_params
      redirect_to manage_klass_path @klass
    else
      render :edit
    end
  end

  def destroy
  end

  private
  def klass_params
    params.require(:klass).permit :name, :start_date, :end_date, :status,
      student_ids: []
  end
end
