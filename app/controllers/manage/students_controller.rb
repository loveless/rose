class Manage::StudentsController < Manage::BaseController

  def index
    @students = Student.all
  end

  def show
    @student = Student.find params[:id]
  end

  def edit
    @student = Student.find params[:id]
  end

  def update
    @student = Student.find params[:id]
    @student.update student_params
    redirect_to manage_student_path @student
  end

  def new
    @student = Student.new
    @klass = Klass.find params[:klass_id] if params[:klass_id]
  end

  def create
    @student = Student.new student_params
    @klass = Klass.find params[:klass_id] if params[:klass_id]
    if @student.save
      if @klass
        redirect_to manage_klass_student_klasses_path @klass
      else
        redirect_to manage_students_path
      end
    else
      render :new
    end
  end

  private
  def student_params
    params.require(:student).permit :name, :email, :phone, :campaign_id, :gender,
      :type, :join_date, :note, student_klasses_attributes: [:klass_id], m_day_ids: []
  end
end
