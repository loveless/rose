class Manage::BaseController < ApplicationController
  layout "manage"
  before_action :authenticate_staff!
end
