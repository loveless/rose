class Manage::PeopleController < Manage::BaseController
  before_action :init_person, only: [:show, :edit, :update]

  Kaminari::Hooks.init
  def index
    params[:q] = {"name_cont" => params[:q]} if params[:q].is_a?(String)
    @q = Person.search params[:q]
    @people = @q.result(distinct: true).page params[:page]
    @all_people = Person.all
    @days = M::Day.all
  end

  def search
    index
    render :index
  end

  def new
    @person = Person.new
    @campaign = Campaign.find params[:campaign_id] if params[:campaign_id]
    @campaign_person = CampaignPerson.new if @campaign
  end

  def create
    @person = Person.new person_params
    @campaign = Campaign.find params[:campaign_id] if params[:campaign_id]
    if @person.save
      if @campaign
        redirect_to manage_campaign_path @campaign
      else
        redirect_to manage_people_path
      end
    else
      render :new
    end
  end

  def show
    @days = M::Day.all
  end

  def edit
    @campaign = Campaign.find params[:campaign_id] if params[:campaign_id]
    if @campaign
      @campaign_person = @person.campaign_people.find_by campaign_id: @campaign.id
    end
  end

  def update
    @campaign = Campaign.find params[:campaign_id] if params[:campaign_id]
    if @person.update person_params
      flash[:success] = "Edit successfully"
      if @campaign
        redirect_to manage_campaign_path @campaign
      else
        redirect_to manage_person_path @person
      end
    else
      render :edit
    end
  end

  def destroy
  end

  private
  def person_params
    params.require(:person).permit :name, :email, :phone, :gender,
      :type, :join_date, :note, campaign_people_attributes: [:id, :campaign_id,
        :note, :is_contacted, m_day_ids: []]
  end

  def init_person
    @person = Person.find params[:id]
  end
end
