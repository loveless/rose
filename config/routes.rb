Rails.application.routes.draw do
  get ":file_name", to: "static_pages#show", constraints: {file_name: 
    /#{Rose::Application::STATIC_PAGES.join("|")}/}
  root "static_pages#home_page"
  devise_for :staffs
  namespace :manage do
    root "people#index"
    resources :campaigns do
      resources :people
    end
    resources :campaign_people, as: :camp_people
    resources :people
    resources :students
    resources :staffs do
      resources :staff_sales
    end
    resources :klasses do
      resources :student_klasses
      resources :lectures
      resources :students
    end
  end
end
